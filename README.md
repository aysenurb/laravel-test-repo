# Proje Görev Açıklamaları #

Aşağıdaki görevler yapılıp repo üzerinde git ile bir **pull-request** oluşturulmalıdır. 
Görevlerden bazılarını tamamladığınızda **farklı bir branch içersine ara commit atabilirsiniz. Lütfen "Master" branch ile merge etmeyiniz ve commitlerde "--force" komutunu kullanmayınız.**

_Kafanıza takılan herhangi bir madde varsa bizimle iletişime geçebilirsiniz._

Projede
- **2 farklı database** üzerinden işlem yapılabilmeli, Birinci database hizmetalanların tablosunu, ikincisi ise hizmetverenlerin tablosunu barındırmalı.

- iki farklı giriş imkanı olan auth sistemi:
  
  - 1)Hizmetalan, 2)Hizmetveren taraf.
  	- a) Hizmetalan bilgileri :
  		* isim soyisim (gerekli),
  		* email (gerekli),
  		* şifre
  		* firmalar (**json formatında kaydedilmeli**)
  	- b) Hizmetveren bilgileri:
  		* firma adı (gerekli),
  		* firma kodu (**kayıtta 12 haneli benzersiz bir kod oluşturulmalı**),
  		* isim soyisim (gerekli),
  		* email (gerekli),
  		* logo ( **minimum 100 X 100** ),
  		* website ( website formatına uygunluk kontrolü )
  		* tanıtım metni ( editör üzerinden değiştirilebilir olmalı. Ve **metnin sonunda proje ENV dosyasında bulunan (APP_KEY)** otomatik olarak yerleştirilebilmeli. )
  		* şifre
  		* müşteriler (**json formatında kaydedilmeli**)
    

- Hizmetalan ve Hizmetveren alanlarının CRUD işlemlerini yapabileceğimiz alanlar

- Hizmetverenler ile hizmetalanlar arasında modellerde birer **relation fonksiyon** bulundurulmalı.

- Sistemde **views klasörü public klasörü altında** çalışacak şekilde ayarlanmalı

- Yukarda oluşturulması istenilen CRUD işlemleri aynı zamanda bir **API servis** altında da çalışabilmeli.

- CRUD'larda yapılan her işlem log olarak kaydedilmeli.

- Hizmetalan ve Hizmetverenlerin son işlem tarihleri
  (**Gün Ay Yıl - Saat:Dakika:Saniye**) Şeklinde gösterilmeli

- Hizmetalanlarda bir test yazılarak oluşturma ve silme alanları test edilebilmeli.

- Oluşturulan views layout üzerinde her sayfanın altında o rotanın **route name'i** gösterilebilmeli.
